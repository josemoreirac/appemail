import 'package:appmailflutter/model/backend.dart';
import 'package:appmailflutter/model/email.dart';
import 'package:appmailflutter/widgets/email_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(        
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  const MyHomePage({Key? key}) : super(key: key);

  @override
  _ListScreenState createState()=> _ListScreenState();
}

class _ListScreenState extends State<MyHomePage> {

  @override
    Widget build(BuildContext context) {
      return Scaffold(
          appBar: AppBar(
            
            title: const Text('App Mail'),
          ),
          body: ListView.builder(
              itemCount: Backend().getEmails().length,
              itemBuilder: (context, index) {
                Email email = Backend().getEmails()[index];
                return Dismissible(
                  key: Key(Backend().getEmails()[index].id.toString()),
                  child: ListTile(
                    subtitle: Text(email.subject),
                    title: Text(email.from),
                    leading: const Icon(Icons.brightness_1, color: Colors.green),
                    
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ContentWidget(email)));
                    },
                    
                  ),
                  onDismissed: (direction){
                    Backend().getEmails().remove(index);
                  },
                );
              }));
    }
}


  

